use std::collections::HashMap;

#[cfg(feature = "log-serialization")]
use std::str::FromStr;

#[cfg(feature = "serialization")]
use serde::{Deserialize, Serialize};

use chrono::{DateTime, Utc};

#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct LogEntry {
    pub timestamp: DateTime<Utc>,
    pub level: String,
    pub target: Option<String>,
    pub span: Option<String>,
    pub message: String,
    pub fields: HashMap<String, String>,
}

#[cfg(feature = "log-serialization")]
#[derive(Debug, Clone, Deserialize)]
struct LogItem {
    pub timestamp: String,
    pub level: String,
    pub target: Option<String>,
    pub span: Option<String>,
    pub fields: HashMap<String, String>,
}

#[cfg(feature = "log-serialization")]
impl FromStr for LogEntry {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut item: LogItem = serde_json::from_str(s)?;
        let message = item.fields.remove("message").unwrap_or_default();
        Ok(Self {
            timestamp: DateTime::parse_from_rfc3339(&item.timestamp)?.into(),
            level: item.level,
            target: item.target,
            span: item.span,
            message,
            fields: item.fields,
        })
    }
}
