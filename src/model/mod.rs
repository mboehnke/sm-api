pub use crate::pmodel::*;

#[cfg(feature = "serialization")]
use serde::{Deserialize, Serialize};

use chrono::{DateTime, Utc};

pub use log::LogEntry;

mod log;

#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Ord)]
pub struct YouTubeVideo {
    pub channel: String,
    pub title: String,
    pub description: String,
    pub upload_date: DateTime<Utc>,
    pub id: String,
}

#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Ord)]
pub struct TwitchVideo {
    pub channel: String,
    pub title: String,
    pub upload_date: DateTime<Utc>,
    pub id: String,
}

#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Ord)]
pub struct Podcast {
    pub channel: String,
    pub title: String,
    pub description: String,
    pub upload_date: DateTime<Utc>,
    pub audio: String,
    pub thumbnail: String,
}

impl From<YouTubeVideo> for YouTubeItem {
    fn from(x: YouTubeVideo) -> Self {
        Self {
            id: x.id,
            upload_date: to_timestamp(x.upload_date),
            channel: x.channel,
            title: x.title,
            description: x.description,
        }
    }
}

impl From<YouTubeItem> for YouTubeVideo {
    fn from(x: YouTubeItem) -> Self {
        Self {
            channel: x.channel,
            title: x.title,
            description: x.description,
            upload_date: from_timestamp(x.upload_date),
            id: x.id,
        }
    }
}

impl From<Podcast> for PodcastItem {
    fn from(x: Podcast) -> Self {
        Self {
            audio: x.audio,
            thumbnail: x.thumbnail,
            upload_date: to_timestamp(x.upload_date),
            channel: x.channel,
            title: x.title,
            description: x.description,
        }
    }
}

impl From<PodcastItem> for Podcast {
    fn from(x: PodcastItem) -> Self {
        Self {
            channel: x.channel,
            title: x.title,
            description: x.description,
            upload_date: from_timestamp(x.upload_date),
            audio: x.audio,
            thumbnail: x.thumbnail,
        }
    }
}

impl From<TwitchVideo> for TwitchItem {
    fn from(x: TwitchVideo) -> Self {
        Self {
            id: x.id,
            upload_date: to_timestamp(x.upload_date),
            channel: x.channel,
            title: x.title,
        }
    }
}

impl From<TwitchItem> for TwitchVideo {
    fn from(x: TwitchItem) -> Self {
        Self {
            channel: x.channel,
            title: x.title,
            upload_date: from_timestamp(x.upload_date),
            id: x.id,
        }
    }
}

fn to_timestamp(date: DateTime<Utc>) -> i64 {
    date.timestamp()
}

fn from_timestamp(secs: i64) -> DateTime<Utc> {
    chrono::TimeZone::timestamp(&Utc, secs, 0)
}
