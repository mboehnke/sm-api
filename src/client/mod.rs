use thiserror::Error;

#[cfg(feature = "datastore-client")]
pub mod datastore;

#[cfg(feature = "scanner-client")]
pub mod scanner;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum ApiClientError {
    #[cfg(feature = "datastore-serialization")]
    #[error("could not deserialize data into {datatype:?}")]
    DeserializationError {
        source: bincode::Error,
        datatype: &'static str,
    },

    #[cfg(feature = "datastore-serialization")]
    #[error("could not serialize data from {datatype:?}")]
    SerializationError {
        source: bincode::Error,
        datatype: &'static str,
    },

    #[error("rpc request failed: {code}")]
    RpcError { source: RpcErrorInner, code: String },
}

#[derive(Error, Debug)]
#[error("{0}")]
pub struct RpcErrorInner(pub String);

impl From<tonic::Status> for ApiClientError {
    fn from(status: tonic::Status) -> Self {
        let message = status.message().to_string();
        let code = status.code().to_string();
        Self::RpcError {
            source: RpcErrorInner(message),
            code,
        }
    }
}
