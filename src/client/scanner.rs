use crate::{
    model::{Podcast, TwitchVideo, YouTubeVideo},
    scanner::{
        podcast_client::PodcastClient, twitch_client::TwitchClient, you_tube_client::YouTubeClient,
        *,
    },
};
use apply::Apply;
use tonic::{
    codegen::StdError,
    transport::{Channel, Endpoint},
};

use crate::client::ApiClientError;

#[derive(Debug, Clone)]
pub struct Scanner {
    pub youtube: YouTubeScanner,
    pub podcast: PodcastScanner,
    pub twitch: TwitchScanner,
}

#[derive(Debug, Clone)]
pub struct YouTubeScanner(YouTubeClient<Channel>);

#[derive(Debug, Clone)]
pub struct TwitchScanner(TwitchClient<Channel>);

#[derive(Debug, Clone)]
pub struct PodcastScanner(PodcastClient<Channel>);

impl Scanner {
    pub async fn connect<T>(dst: T) -> Result<Self, tonic::transport::Error>
    where
        T: TryInto<Endpoint> + Clone,
        T::Error: Into<StdError>,
    {
        let youtube = YouTubeClient::connect(dst.clone())
            .await?
            .apply(YouTubeScanner);
        let podcast = PodcastClient::connect(dst.clone())
            .await?
            .apply(PodcastScanner);
        let twitch = TwitchClient::connect(dst).await?.apply(TwitchScanner);
        Ok(Self {
            youtube,
            podcast,
            twitch,
        })
    }
}

impl TwitchScanner {
    pub async fn uploads(
        &mut self,
        channel_id: &str,
        max_days: u32,
        max_num: u32,
    ) -> Result<Vec<TwitchVideo>, ApiClientError> {
        let request = TwitchUploadsRequest {
            channel_id: channel_id.into(),
            max_days,
            max_num,
        };
        let TwitchUploadsReply { items } = self.0.uploads(request).await?.into_inner();
        let videos = items.into_iter().map(TwitchVideo::from).collect();
        Ok(videos)
    }
}

impl PodcastScanner {
    pub async fn uploads(
        &mut self,
        url: &str,
        max_days: u32,
        max_num: u32,
    ) -> Result<Vec<Podcast>, ApiClientError> {
        let request = PodcastUploadsRequest {
            url: url.into(),
            max_days,
            max_num,
        };
        let PodcastUploadsReply { items } = self.0.uploads(request).await?.into_inner();
        let videos = items.into_iter().map(Podcast::from).collect();
        Ok(videos)
    }
}

impl YouTubeScanner {
    pub async fn uploads(
        &mut self,
        channel_id: &str,
        max_days: u32,
        max_num: u32,
    ) -> Result<Vec<YouTubeVideo>, ApiClientError> {
        let request = YouTubeUploadsRequest {
            channel_id: channel_id.into(),
            max_days,
            max_num,
        };
        let YouTubeUploadsReply { items } = self.0.uploads(request).await?.into_inner();
        let videos = items.into_iter().map(YouTubeVideo::from).collect();
        Ok(videos)
    }

    pub async fn subscribed_channels(&mut self) -> Result<Vec<String>, ApiClientError> {
        let request = YouTubeSubscribedChannelsRequest {};
        let YouTubeSubscribedChannelsReply { channel_ids } = self
            .0
            .clone()
            .subscribed_channels(request)
            .await?
            .into_inner();
        Ok(channel_ids)
    }

    pub async fn playlist(
        &mut self,
        playlist_name: &str,
    ) -> Result<Vec<(String, YouTubeVideo)>, ApiClientError> {
        let request = YouTubePlaylistRequest {
            playlist_name: playlist_name.into(),
        };
        let YouTubePlaylistReply { items } = self.0.playlist(request).await?.into_inner();
        let videos = items
            .into_iter()
            .filter_map(
                |YouTubePlaylistItem {
                     playlist_item_id,
                     item,
                 }| Some((playlist_item_id, YouTubeVideo::from(item?))),
            )
            .collect();
        Ok(videos)
    }

    pub async fn remove_playlist_item(
        &mut self,
        playlist_item_id: &str,
    ) -> Result<(), ApiClientError> {
        let request = YouTubeRemovePlaylistItemRequest {
            playlist_item_id: playlist_item_id.into(),
        };
        let YouTubeRemovePlaylistItemReply {} =
            self.0.remove_playlist_item(request).await?.into_inner();
        Ok(())
    }

    pub async fn all_uploads(
        &mut self,
        max_days: u32,
        max_num_per_channel: u32,
    ) -> Result<Vec<YouTubeVideo>, ApiClientError> {
        let request = YouTubeAllUploadsRequest {
            max_days,
            max_num_per_channel,
        };
        let YouTubeAllUploadsReply { items } = self.0.all_uploads(request).await?.into_inner();
        let videos = items.into_iter().map(YouTubeVideo::from).collect();
        Ok(videos)
    }

    pub async fn set_client_secret(&mut self, secret: &str) -> Result<(), ApiClientError> {
        let request = YouTubeSetClientSecretRequest {
            secret: secret.into(),
        };
        self.0.set_client_secret(request).await?;
        Ok(())
    }
}
