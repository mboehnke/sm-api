use crate::datastore::{datastore_client::DatastoreClient, *};
use tonic::{codegen::StdError, transport::Channel};

use crate::client::ApiClientError;

#[cfg(feature = "datastore-writer")]
pub use writer::DatastoreWriter;

#[cfg(feature = "datastore-writer")]
mod writer;

#[cfg(feature = "datastore-serialization")]
mod serialization;

#[derive(Debug, Clone)]
pub struct Datastore(DatastoreClient<Channel>);

#[derive(Debug, Clone)]
pub struct Tree {
    client: DatastoreClient<Channel>,
    tree: String,
}

impl Datastore {
    pub async fn connect<T>(dst: T) -> Result<Self, tonic::transport::Error>
    where
        T: std::convert::TryInto<tonic::transport::Endpoint>,
        T::Error: Into<StdError>,
    {
        DatastoreClient::connect(dst).await.map(Self)
    }

    pub fn tree<T: std::fmt::Display>(&self, tree: T) -> Tree {
        Tree {
            client: self.0.clone(),
            tree: tree.to_string(),
        }
    }
}

impl Tree {
    /// Retrieve a value from the `Tree` if it exists.
    pub async fn get_raw<K>(&self, key: K) -> Result<Option<Vec<u8>>, ApiClientError>
    where
        K: AsRef<[u8]>,
    {
        let tree = self.tree.clone();
        let key = key.as_ref().to_vec();
        let GetReply { value } = self
            .client
            .clone()
            .get(GetRequest { tree, key })
            .await?
            .into_inner();
        Ok(value)
    }

    /// Retrieve all key/value pairs from the `Tree`.
    pub async fn get_all_raw(&self) -> Result<Vec<(Vec<u8>, Vec<u8>)>, ApiClientError> {
        let tree = self.tree.clone();
        let GetAllReply { items } = self
            .client
            .clone()
            .get_all(GetAllRequest { tree })
            .await?
            .into_inner();
        let items = items
            .into_iter()
            .map(|KvPair { key, value }| (key, value))
            .collect();
        Ok(items)
    }

    /// Retrieve all keys from the `Tree`.
    pub async fn keys(&self) -> Result<Vec<Vec<u8>>, ApiClientError> {
        let tree = self.tree.clone();
        let KeysReply { keys } = self
            .client
            .clone()
            .keys(KeysRequest { tree })
            .await?
            .into_inner();
        Ok(keys)
    }

    /// Retrieve all values from the `Tree`.
    pub async fn values_raw(&self) -> Result<Vec<Vec<u8>>, ApiClientError> {
        let tree = self.tree.clone();
        let ValuesReply { values } = self
            .client
            .clone()
            .values(ValuesRequest { tree })
            .await?
            .into_inner();
        Ok(values)
    }

    pub async fn remove_raw<K>(&self, key: K) -> Result<Option<Vec<u8>>, ApiClientError>
    where
        K: AsRef<[u8]>,
    {
        let tree = self.tree.clone();
        let key = key.as_ref().to_vec();
        let RemoveReply { previous_value } = self
            .client
            .clone()
            .remove(RemoveRequest { tree, key })
            .await?
            .into_inner();
        Ok(previous_value)
    }

    /// Insert a key to a new value, returning the last value if it was set.
    pub async fn insert_raw<K, V>(
        &self,
        key: K,
        value: V,
    ) -> Result<Option<Vec<u8>>, ApiClientError>
    where
        K: AsRef<[u8]>,
        V: AsRef<[u8]>,
    {
        let tree = self.tree.clone();
        let key = key.as_ref().to_vec();
        let value = value.as_ref().into();
        let InsertReply { previous_value } = self
            .client
            .clone()
            .insert(InsertRequest { tree, key, value })
            .await?
            .into_inner();
        Ok(previous_value)
    }

    /// Insert a key to an empty value, returning the last value if it was set.
    pub async fn insert_key<K>(&self, key: K) -> Result<Option<Vec<u8>>, ApiClientError>
    where
        K: AsRef<[u8]>,
    {
        let tree = self.tree.clone();
        let key = key.as_ref().to_vec();
        let InsertKeyReply { previous_value } = self
            .client
            .clone()
            .insert_key(InsertKeyRequest { tree, key })
            .await?
            .into_inner();
        Ok(previous_value)
    }

    /// Insert a value into the `Tree`.
    ///
    /// This generates an ID and uses it as key.
    /// IDs are guaranteed to be unique but may still clash with manually defined keys.
    pub async fn insert_value_raw<V>(&self, value: V) -> Result<(), ApiClientError>
    where
        V: AsRef<[u8]>,
    {
        let tree = self.tree.clone();
        let value = value.as_ref().into();
        self.client
            .clone()
            .insert_value(InsertValueRequest { tree, value })
            .await?
            .into_inner();
        Ok(())
    }

    /// Retrieve a value from the `Tree` if it exists.
    ///
    /// Otherwise insert the default value and return that.
    pub async fn get_or_insert_raw<K, V>(
        &self,
        key: K,
        default: V,
    ) -> Result<Vec<u8>, ApiClientError>
    where
        K: AsRef<[u8]>,
        V: AsRef<[u8]>,
    {
        let tree = self.tree.clone();
        let key = key.as_ref().to_vec();
        let default_value = default.as_ref().into();
        let GetOrInsertReply { value } = self
            .client
            .clone()
            .get_or_insert(GetOrInsertRequest {
                tree,
                key,
                default_value,
            })
            .await?
            .into_inner();
        Ok(value)
    }

    /// Swap the values of two keys, returning true if any changes were made.
    ///
    /// If one or both key(s) don't exist, this will result in a deletion.
    pub async fn swap<K1, K2>(&self, key1: K1, key2: K2) -> Result<bool, ApiClientError>
    where
        K1: AsRef<[u8]>,
        K2: AsRef<[u8]>,
    {
        let tree = self.tree.clone();
        let key1 = key1.as_ref().to_vec();
        let key2 = key2.as_ref().to_vec();
        let SwapReply { swapped } = self
            .client
            .clone()
            .swap(SwapRequest { tree, key1, key2 })
            .await?
            .into_inner();
        Ok(swapped)
    }
}
