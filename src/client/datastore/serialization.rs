use std::any::type_name;

use serde::{de::DeserializeOwned, Serialize};

use crate::client::ApiClientError;

use super::Tree;

impl Tree {
    /// Retrieve a value from the `Tree` if it exists.
    ///
    /// Attempts to (de)serialize data as needed.
    pub async fn get<K, V>(&self, key: K) -> Result<Option<V>, ApiClientError>
    where
        K: AsRef<[u8]>,
        V: DeserializeOwned,
    {
        self.get_raw(key).await?.map(deserialize).transpose()
    }

    /// Retrieve all key/value pairs from the `Tree`.
    ///
    /// Attempts to (de)serialize data as needed.
    pub async fn get_all<V>(&self) -> Result<Vec<(Vec<u8>, V)>, ApiClientError>
    where
        V: DeserializeOwned,
    {
        self.get_all_raw()
            .await?
            .into_iter()
            .map(|(key, value)| deserialize(value).map(move |v| (key, v)))
            .collect()
    }

    /// Retrieve all values from the `Tree`.
    ///
    /// Attempts to (de)serialize data as needed.
    pub async fn values<V>(&self) -> Result<Vec<V>, ApiClientError>
    where
        V: DeserializeOwned,
    {
        self.values_raw()
            .await?
            .into_iter()
            .map(deserialize)
            .collect()
    }

    /// Insert a key to a new value, returning the last value if it was set.
    ///
    /// Attempts to (de)serialize data as needed.
    pub async fn insert<K, V>(&self, key: K, value: V) -> Result<Option<V>, ApiClientError>
    where
        K: AsRef<[u8]>,
        V: DeserializeOwned + Serialize,
    {
        let value = serialize(&value)?;
        self.insert_raw(key, value)
            .await?
            .map(deserialize)
            .transpose()
    }

    /// Insert a value into the `Tree`.
    ///
    /// This generates an ID and uses it as key.
    /// IDs are guaranteed to be unique but may still clash with manually defined keys.
    ///
    /// Attempts to (de)serialize data as needed.
    pub async fn insert_value<V>(&self, value: V) -> Result<(), ApiClientError>
    where
        V: Serialize,
    {
        let value = serialize(&value)?;
        self.insert_value_raw(value).await
    }

    /// Retrieve a value from the `Tree` if it exists.
    ///
    /// Otherwise insert the default value and return that.
    ///
    /// Attempts to (de)serialize data as needed.
    pub async fn get_or_insert<K, V>(&self, key: K, default: V) -> Result<V, ApiClientError>
    where
        K: AsRef<[u8]>,
        V: Serialize + DeserializeOwned,
    {
        let default = serialize(&default)?;
        let bytes = self.get_or_insert_raw(key, default).await?;
        let value = deserialize(bytes)?;
        Ok(value)
    }

    /// Delete a value, returning the old value if it existed.
    ///
    /// Attempts to (de)serialize data as needed.
    pub async fn remove<K, V>(&self, key: K) -> Result<Option<V>, ApiClientError>
    where
        K: AsRef<[u8]>,
        V: DeserializeOwned,
    {
        if let Some(previous_value) = self.remove_raw(key).await? {
            let previous_value = deserialize(previous_value)?;
            Ok(Some(previous_value))
        } else {
            Ok(None)
        }
    }
}

pub fn serialize<T: Serialize>(value: &T) -> Result<Vec<u8>, ApiClientError> {
    bincode::serialize(value).map_err(|source| ApiClientError::SerializationError {
        source,
        datatype: type_name::<T>(),
    })
}

pub fn deserialize<T: DeserializeOwned>(bytes: impl AsRef<[u8]>) -> Result<T, ApiClientError> {
    bincode::deserialize(bytes.as_ref()).map_err(|source| ApiClientError::DeserializationError {
        source,
        datatype: type_name::<T>(),
    })
}
