use std::{io::Write, sync::Arc};

use tokio::runtime::Runtime;

use super::Tree;

#[derive(Clone, Debug)]
pub struct DatastoreWriter {
    runtime: Arc<Runtime>,
    tree: Tree,
}

impl DatastoreWriter {
    pub fn new(tree: Tree) -> std::io::Result<Self> {
        let runtime = Arc::new(Runtime::new()?);
        Ok(Self { runtime, tree })
    }
}

impl Write for DatastoreWriter {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        self.runtime
            .block_on(async { self.tree.insert_value_raw(buf).await })
            .map(|_| buf.len())
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}
