use std::{fs::File, io::Write};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("cargo:rerun-if-changed=proto");

    let protos = [
        "proto/datastore.proto",
        "proto/pmodel.proto",
        "proto/scanner.proto",
    ];
    let out_dir = std::env::var("OUT_DIR")?;
    let enable_client = std::env::var("CARGO_FEATURE_CLIENT").is_ok();
    let enable_server = std::env::var("CARGO_FEATURE_SERVER").is_ok();

    tonic_build::configure()
        .build_client(enable_client)
        .build_server(enable_server)
        .compile(&protos, &["proto"])?;

    let mut f = File::create("src/lib.rs")?;
    writeln!(
        f,
        "//! AUTOMATICALLY GENERATED CODE\n\
        //! DO NOT EDIT\n\
        \n\
        #[path = \"{out_dir}/datastore.rs\"]\n\
        pub mod datastore;\n\
        \n\
        #[path = \"{out_dir}/scanner.rs\"]\n\
        pub mod scanner;\n\
        \n\
        #[path = \"{out_dir}/pmodel.rs\"]\n\
        mod pmodel;\n\
        \n\
        #[cfg(feature = \"model\")]\n\
        pub mod model;\n\
        \n\
        #[cfg(feature = \"client\")]\n\
        pub mod client;"
    )?;

    Ok(())
}
